ROM_NAME = hello

#use default FamiTone settings

include makerules


.SUFFIXES: .c .o .rc .s

.PHONY : all clean

all:  $(ROM)

$(ROM): $(OBJ_FILES) $(MARIA_OBJS)
#	@echo $(OBJ_FILES)
#	@echo $(MARIA_OBJS)
	$(LD) $(LDFLAGS) -C $(MARIA_PATH)/config/nrom_256_horz.cfg -o $@ $(OBJ_FILES) $(MARIA_OBJS) nes.lib

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
#this doesn't work on Windows :(
	-$(MD) -p $(@D)
	$(CC) $(CFLAGS) -o $@ $<

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.s 
#this doesn't work on Windows :(
	-$(MD) -p $(@D)
	$(AS) $(ASMFLAGS) -o $@ $<

	
$(OBJ_DIR)/%.o : $(SRC_DIR)/%.rc 
	$(RESC) $< $<.s 
	$(AS) $(ASMFLAGS) -o $@ $<.s

$(MARIA_OBJ_DIR)/%.o : $(MARIA_SRC_DIR)/%.s 
#this doesn't work on Windows :(
	-$(MD) -p $(@D)
	$(AS) $(ASMFLAGS) -o $@ $<

	
clean:
	-$(RMD) $(OBJ_DIR)
	-$(RM) $(ROM)
	-$(RM) $(ROM_NAME)_labels.txt $(ROM_NAME)_map.txt $(ROM_NAME).dbg

.PRECIOUS: resource.s
